function deepCopy(obj, copiedRefs = new WeakMap()) {
    if (copiedRefs.has(obj)) {
        return copiedRefs.get(obj);
    }
    
    if (obj === null || typeof obj !== "object") {
        return obj;
    }

    if (Array.isArray(obj)) {
        const copy = [];
        copiedRefs.set(obj, copy);
        obj.forEach((item, idx) => copy[idx] = deepCopy(item, copiedRefs));

        return copy;
    } 
    
    if (obj instanceof Map) {
        const copy = new Map();
        copiedRefs.set(obj, copy);
        for (let key of obj.keys()) {
            copy.set(deepCopy(key, copiedRefs), deepCopy(obj.get(key), copiedRefs));
        }

        return copy;
    } 
    
    if (obj instanceof Date) {
        return new Date(obj);
    }

    if (obj instanceof Set) {
        const copy = new Set();
        copiedRefs.set(obj, copy);
        for (const item of obj) {
            copy.add(deepCopy(item, copiedRefs));
        }

        return copy;
    } 
    
    if (typeof obj === "object") {
        const copy = Object.create(Object.getPrototypeOf(obj));

        copiedRefs.set(obj, copy);

        for (const prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                copy[prop] = deepCopy(obj[prop], copiedRefs);
            }
        }

        return copy;
    }     
}

const originalObject = {
    number: 1,
    array: [
        {
            a: {
                b: {
                    c: 5
                }
            }
        },
        "hello",
        new Set([1, 2, 3]),
    ],
    fn: function() {console.log("fn");},
    sym: Symbol(),
    date: new Date(new Date().setFullYear(2000)),
    set: new Set([1, 2, [1,4]]),
    map: new Map([
        ["1", "one"],
        [2, "two"],
        [3, {3: "three"}],
        [4, function() {}],
      ]),
};


originalObject["self"] = originalObject;
originalObject.array.push(originalObject.array);

let copyObject = deepCopy(originalObject);

console.log("Original: ", originalObject);
console.log("Clone: ", copyObject);
