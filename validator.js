function isValid(s) {
    const stack = [];
    const matchBrackets = {
        '(': ')',
        '[': ']',
        '{': '}'
    };

    for (let i = 0; i < s.length; i++) {
        const current = s[i];
        if (matchBrackets[current]) {
            stack.push(current);
        } else {
            lastClose = stack.pop();
            if (current !== matchBrackets[lastClose]) return false;
        }
    }

    return stack.length === 0;
}


console.log("()\t:", isValid("()"));
console.log("()[]{}\t:", isValid("()[]{}"));
console.log("(]\t:", isValid("(]"));
console.log("([)]\t:", isValid("([)]"));
console.log("{[]}\t:", isValid("{[]}"));

let bracketsString = "";

for (let i = 0; i < 10_000; i++) {
    if (i < 5000) {
        bracketsString += '{'
    } else {
        bracketsString += '}'
    }
}

console.log("valid 10^4 string:", isValid(bracketsString));

