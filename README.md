## Задача на создание дома используя HTML и CSS
[`index.html`](./index.html)
[`index.css`](./index.css)

## Задача на написание функции глубокого копирования
[`deepCopy.js`](./deepCopy.js)

## Задача на функцию валидации строки из скобок
[`validator.js`](./validator.js)